import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from './api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  cars: any;
  carsAll:any;
  constructor(private modalService: NgbModal, public api: ApiService) {

  }

  ngOnInit() {
    this.loadData()
  }

  

  loadData() {
    this.api.getHeaders("vehiculos").subscribe((response: any) => {
      this.cars = response.data;
      this.carsAll = response.data;
    })
  }
  title = 'acme';
  model = {
    left: true,
    middle: false,
    right: false
  };

  tabs = [
    { title: "Personas", id: 1 },
    { title: "Vehiculos", id: 2 },
    { title: "Vehiculos", id: 2 },
    { title: "Vehiculos", id: 2 },
    { title: "Vehiculos", id: 2 },
  ];
  counter = this.tabs.length + 1;
  active: any = 'Vehiculos';
  closeResult = '';
  changeCount = 0;
  activeCar = {
    "placa": "BMW1",
  }

  applyFilter(event: any) {
    console.log(event.target.value);
    if (event.target.value == "") {
      this.loadData();
    } else {
      this.cars = this.carsAll.filter((val: any) => {
        return val.placa.toLowerCase().indexOf(event.target.value) > -1;
      });
    }
  }


  open(content: any, car: any) {
    this.activeCar = car;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
