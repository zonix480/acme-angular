import { HttpClient, HttpRequest, HttpEventType, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../environments/environment.prod";
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public ENV = environment.apiUrl;
  constructor(public http: HttpClient
  ) { }

  /*---------------------
REQUESTS WITH HEADERS
---------------------*/
  /**
   * Petition GET with Headers "Bearer auth"
   * @param
   */
  public getHeaders(url: string): Observable<any> {
    var fullUrl = url;
    return this.http.get(`${this.ENV}${fullUrl}`, { headers: this.setHeaders() });
  }

  /**
 * Set Headers
 * @param error
 */
  public setHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }


}
